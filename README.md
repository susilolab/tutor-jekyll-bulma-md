# Jekyll Bulma MD Test Drive

An example of Jekyll site using Bulma Material Design
for personal learning purpose.

> Jekyll (Liquid) + Bulma (Material Design)

![Jekyll Bulma: Tutor][jekyll-bulma-preview]


-- -- --

## Jekyll Version

Since this repository is still using Jekyll 3.8, instead of Jekyll 4.0,
you might need to run `bundle install`, depend on the situation.

	bundle install --full-index

-- -- --

## Links

### Jekyll Step By Step

This repository:

* [Jekyll Step by Step Repository][tutorial-jekyll]

### HTML Step By Step

Additional guidance:

* [Bulma Material Design Step by Step Repository][tutorial-bulma-md] (frankenbulma)

* [Bulma Step by Step Repository][tutorial-bulma]

* [Materialize Step by Step Repository][tutorial-materialize]

### Comparison

Comparation with other static site generator

* [Eleventy (Materialize) Step by Step Repository][tutorial-11ty-m]

* [Eleventy (Bulma MD) Step by Step Repository][tutorial-11ty-b]]

* [Hugo Step by Step Repository][tutorial-hugo]

* [Hexo Step by Step Repository][tutorial-hexo]

* [Pelican Step by Step Repository][tutorial-pelican]

### Presentation

* [Concept SSG - Presentation Slide][ssg-presentation]

* [Concept CSS - Presentation Slide][css-presentation]

[tutorial-pelican]:     https://gitlab.com/epsi-rns/tutor-pelican-bulma-md/
[tutorial-jekyll]:      https://gitlab.com/epsi-rns/tutor-jekyll-bulma-md/
[tutorial-hugo]:        https://gitlab.com/epsi-rns/tutor-hugo-bulma-md/
[tutorial-11ty-m]:      https://gitlab.com/epsi-rns/tutor-11ty-materialize/
[tutorial-11ty-b]:      https://gitlab.com/epsi-rns/tutor-11ty-bulma-md/
[tutorial-hexo]:        https://gitlab.com/epsi-rns/tutor-hexo-bulma/

[tutorial-bulma-md]:    https://gitlab.com/epsi-rns/bulma-material-design/
[tutorial-bulma]:       https://gitlab.com/epsi-rns/tutor-html-bulma/
[tutorial-materialize]: https://gitlab.com/epsi-rns/tutor-html-materialize/

[ssg-presentation]:     https://epsi-rns.gitlab.io/ssg/2019/02/17/concept-ssg/
[css-presentation]:     https://epsi-rns.gitlab.io/frontend/2019/02/15/concept-css/

-- -- --

## Chapter Step by Step

### Tutor 01

> Generate Only Pure HTML

* Setup Directory for Minimal Jekyll

* General Layout: Default, Page, Post, Home

* Partials: HTML Head, Header, Footer

* Basic Content

![Jekyll Bulma: Tutor 01][jekyll-bulma-preview-01]

-- -- --

### Tutor 02

> Add Bulma CSS Framework

* Add Bulma CSS

* Standard Header (jquery or vue) and Footer

* Enhance All Layouts with Bulma CSS

* Apply Bulma Two Column Responsive Layout for Most Layout

![Jekyll Bulma: Tutor 02][jekyll-bulma-preview-02]

-- -- --

### Tutor 03

> Combine with Custom SASS Material Design

* Add (a bunch of) Custom SASS (Custom Design)

* Nice Header and Footer

* Index Content: Index, Category, Tag, Archive By Year/Month

* Enhance All Two Column Responsive Layout with Material Design

* Nice Tag Badge in Tags Page

![Jekyll Bulma: Tutor 03][jekyll-bulma-preview-03]

-- -- --

### Tutor 04

> Loop with Liquid

* More Content: Lyrics and Quotes. Need this content for demo

* Simplified All Layout Using Liquid Template Inheritance

* List Tree: Archives: By Year, By Year and Month

* List Tree: Tags and Categories

* Article Index: By Year, List Tree (By Year and Month)

* Custom Output: YAML, TXT, JSON

![Jekyll Bulma: Tutor 04][jekyll-bulma-preview-04]

-- -- --

### Tutor 05

> Features

* Widget: Friends, Archives Tree, Categories, Tags, Recent Post, Related Post

* Refactoring Template using Capture: Widget

* Pagination (v1): Adjacent, Indicator, Responsive

* Pagination (v2): Adjacent, Indicator, Responsive

* Post: Header, Footer, Navigation

![Jekyll Bulma: Tutor 05][jekyll-bulma-preview-05]

-- -- --

### Tutor 06

> Finishing

* Layout: Service (dummy)

* Post: Markdown Content (test case)

* Post: Table of Content (dynamic include)

* Post: Responsive Images

* Post: Syntax Highlight (sass)

* Post: Shortcodes (include with parameter)

* Official Plugin: Feed

* Meta: HTML, SEO, Opengraph, Twitter

* Multi Column Responsive List: Categories, Tags, and Archives

![Jekyll Bulma: Tutor 06][jekyll-bulma-preview-06]

-- -- --

What do you think ?

[jekyll-bulma-preview]:     https://gitlab.com/epsi-rns/tutor-jekyll-bulma-md/raw/master/jekyll-bulma-md-preview.png
[jekyll-bulma-preview-01]:  https://gitlab.com/epsi-rns/tutor-jekyll-bulma-md/raw/master/tutor-01/jekyll-bulma-md-preview.png
[jekyll-bulma-preview-02]:  https://gitlab.com/epsi-rns/tutor-jekyll-bulma-md/raw/master/tutor-02/jekyll-bulma-md-preview.png
[jekyll-bulma-preview-03]:  https://gitlab.com/epsi-rns/tutor-jekyll-bulma-md/raw/master/tutor-03/jekyll-bulma-md-preview.png
[jekyll-bulma-preview-04]:  https://gitlab.com/epsi-rns/tutor-jekyll-bulma-md/raw/master/tutor-04/jekyll-bulma-md-preview.png
[jekyll-bulma-preview-05]:  https://gitlab.com/epsi-rns/tutor-jekyll-bulma-md/raw/master/tutor-05/jekyll-bulma-md-preview.png
[jekyll-bulma-preview-06]:  https://gitlab.com/epsi-rns/tutor-jekyll-bulma-md/raw/master/tutor-06/jekyll-bulma-md-preview.png
