### Tutor 01

> Generate Only Pure HTML

* Setup Directory for Minimal Jekyll

* General Layout: Default, Page, Post, Home

* Partials: HTML Head, Header, Footer

* Basic Content

![Jekyll Bulma: Tutor 01][jekyll-bulma-preview-01]

-- -- --

What do you think ?

[jekyll-bulma-preview-01]:  https://gitlab.com/epsi-rns/tutor-jekyll-bulma-md/raw/master/tutor-01/jekyll-bulma-md-preview.png
