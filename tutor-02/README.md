### Tutor 02

> Add Bulma CSS Framework

* Add Bulma CSS

* Standard Header (jquery or vue) and Footer

* Enhance All Layouts with Bulma CSS

* Apply Bulma Two Column Responsive Layout for Most Layout

![Jekyll Bulma: Tutor 02][jekyll-bulma-preview-02]

-- -- --

What do you think ?

[jekyll-bulma-preview-02]:  https://gitlab.com/epsi-rns/tutor-jekyll-bulma-md/raw/master/tutor-02/jekyll-bulma-md-preview.png
