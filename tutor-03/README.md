### Tutor 03

> Combine with Custom SASS Material Design

* Add (a bunch of) Custom SASS (Custom Design)

* Nice Header and Footer

* Index Content: Index, Category, Tag, Archive By Year/Month

* Enhance All Two Column Responsive Layout with Material Design

* Nice Tag Badge in Tags Page

![Jekyll Bulma: Tutor 03][jekyll-bulma-preview-03]

-- -- --

What do you think ?

[jekyll-bulma-preview-03]:  https://gitlab.com/epsi-rns/tutor-jekyll-bulma-md/raw/master/tutor-03/jekyll-bulma-md-preview.png
