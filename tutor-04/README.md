### Tutor 04

> Loop with Liquid

* More Content: Lyrics and Quotes. Need this content for demo

* Simplified All Layout Using Liquid Template Inheritance

* List Tree: Archives: By Year, By Year and Month

* List Tree: Tags and Categories

* Dynamic Include: Blog Header

* Article Index: By Year, List Tree (By Year and Month)

* Custom Output: YAML, TXT, JSON

![Jekyll Bulma: Tutor 04][jekyll-bulma-preview-04]

-- -- --

What do you think ?

[jekyll-bulma-preview-04]:  https://gitlab.com/epsi-rns/tutor-jekyll-bulma-md/raw/master/tutor-04/jekyll-bulma-md-preview.png
