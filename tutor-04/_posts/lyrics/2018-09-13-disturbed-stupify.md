---
layout      : post
title       : Disturbed - Stupify
date        : 2018-09-13 07:35:05 +0700
categories  : lyric
tags        : [nu metal, 2000s]
---

Look in my face, stare in my soul

Why, do you like playing around with  
My, narrow scope of reality  
I, can feel it all start slipping  
I think I'm breaking down

Look in my face, stare into my soul  
I begin to stupify
