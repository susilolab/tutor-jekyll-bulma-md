### Tutor 05

> Features

* Widget: Friends, Archives Tree, Categories, Tags, Recent Post, Related Post

* Refactoring Template using Capture: Widget

* Pagination (v1): Adjacent, Indicator, Responsive

* Pagination (v2): Adjacent, Indicator, Responsive

* Post: Header, Footer, Navigation

![Jekyll Bulma: Tutor 05][jekyll-bulma-preview-05]

-- -- --

What do you think ?

[jekyll-bulma-preview-05]:  https://gitlab.com/epsi-rns/tutor-jekyll-bulma-md/raw/master/tutor-05/jekyll-bulma-md-preview.png
