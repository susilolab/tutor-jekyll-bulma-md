---
layout      : post
title       : House of Pain - Jump Around
date        : 2018-01-15 07:35:05 +0700
categories  : lyric
tags        : [hip hop, 90s]
author      : epsi
---

Pack it up, pack it in, let me begin  
I came to win, battle me that's a sin

Get up, stand up (c'mon!) see'mon throw your hands up  
If you've got the feeling, jump across the ceiling

Feelin', funkin', amps in the trunk and I got more rhymes
