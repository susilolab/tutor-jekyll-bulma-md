---
layout      : post
title       : RATM - Bulls on Parade
date        : 2018-02-15 07:35:05 +0700
categories  : lyric
tags        : [nu metal, 90s]
author      : epsi
---

Weapons, not food, not homes, not shoes  
Not need, just feed the war cannibal animal

While arms warehouses fill as quick as the cells  
Rally round tha family, pocket full of shells
