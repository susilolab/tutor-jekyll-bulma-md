---
layout      : post
title       : Soulfly - Jump Da Fuck Up
date        : 2018-09-07 07:35:05 +0700
categories  : lyric
tags        : [nu metal, 2000s]
author      : epsi
---

You seem to sever all my frequencies  
I'm tethered to your energies

And everything turns inside out  
I can't be killed but I'm not too proud

All this is making things a bit insane  
And I don't care who stares or stays  
The only thing that matters is  
Will you reach out if you can't resist?
