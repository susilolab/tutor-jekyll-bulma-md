### Tutor 06

> Finishing

* Layout: Service (dummy)

* Post: Markdown Content (test case)

* Post: Table of Content

* Post: Responsive Images

* Post: Syntax Highlight (sass)

* Post: Shortcodes (include with parameter)

* Official Plugin: Feed

* Meta: HTML, SEO, Opengraph, Twitter

* Multi Column Responsive List: Categories, Tags, and Archives

![Jekyll Bulma: Tutor 06][jekyll-bulma-preview-06]

-- -- --

What do you think ?

[jekyll-bulma-preview-06]:  https://gitlab.com/epsi-rns/tutor-jekyll-bulma-md/raw/master/tutor-06/jekyll-bulma-md-preview.png
